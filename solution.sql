-- Given the following ERD, create a solution.sql and write the SQL code necessary to create the shown tables and images.
-- ● Create a enrollment_db database.
-- ● Note: Also include the primary key and foreign key constraints in your
-- answer.

CREATE DATABASE enrollment_db;

USE enrollment_db;

CREATE TABLE teachers (
    teacher_id INT NOT NULL AUTO_INCREMENT,
    teacher_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (teacher_id)
);

CREATE TABLE students (
    student_id INT NOT NULL AUTO_INCREMENT,
    student_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (student_id)
);

CREATE TABLE courses (
    course_id INT NOT NULL AUTO_INCREMENT,
    course_name VARCHAR(50) NOT NULL,
    teacher_id INT NOT NULL,
    PRIMARY KEY (course_id),
    CONSTRAINT fk_courses_teacher_id
        FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE students_courses (
    id INT NOT NULL AUTO_INCREMENT,
    course_id INT NOT NULL,
    student_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_students_courses_course_id
        FOREIGN KEY (course_id) REFERENCES courses(course_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_students_courses_student_id
        FOREIGN KEY (student_id) REFERENCES students(student_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

